﻿// <copyright file="ScoresVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using DrunkenRush.Repository;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Scores View Model.
    /// </summary>
    public class ScoresVM : ViewModelBase
    {
        /// <summary>
        /// repository.
        /// </summary>
        private IStorageRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoresVM"/> class.
        /// </summary>
        /// <param name="repository">highscore repository.</param>
        public ScoresVM(IStorageRepository repository)
        {
            this.repository = repository;
            this.Highscores = new ObservableCollection<HighscoreVM>();
            this.LoadScores();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoresVM"/> class.
        /// </summary>
        public ScoresVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IStorageRepository>())
        {
        }

        /// <summary>
        /// Gets scores.
        /// </summary>
        public ObservableCollection<HighscoreVM> Highscores { get; private set; }

        private void LoadScores()
        {
            var scores = this.repository.GetScores();
            if (scores == null)
            {
                return;
            }
            else
            {
                foreach (var player in scores)
                {
                    if (string.IsNullOrEmpty(player.Element("name").Value))
                    {
                        this.Highscores.Add(new HighscoreVM("Anonymous", int.Parse(player.Element("score").Value)));
                    }
                    else
                    {
                        this.Highscores.Add(new HighscoreVM(player.Element("name").Value, int.Parse(player.Element("score").Value)));
                    }
                }
            }
        }
    }
}
