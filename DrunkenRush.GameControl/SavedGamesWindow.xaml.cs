﻿// <copyright file="SavedGamesWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for SavedGamesWindow.xaml.
    /// </summary>
    public partial class SavedGamesWindow : Window
    {
        private SavedGameVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="SavedGamesWindow"/> class.
        /// </summary>
        public SavedGamesWindow()
        {
            this.InitializeComponent();
            this.vm = this.FindResource("VM") as SavedGameVM;
        }

        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string name = ((sender as ListBox).SelectedItem as HighscoreVM)?.Name;
            string tempscore = ((sender as ListBox).SelectedItem as HighscoreVM)?.Score.ToString(CultureInfo.CurrentCulture);

            int score = string.IsNullOrEmpty(tempscore) ? 0 : int.Parse(tempscore);

            this.vm.DeleteSelectedSave(name, score);

            MainWindow mainWindow = new MainWindow(name, score);
            mainWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.Close();
            mainWindow.Show();
        }
    }
}
