﻿// <copyright file="MenuWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for MenuWindow.xaml.
    /// </summary>
    public partial class MenuWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuWindow"/> class.
        /// </summary>
        public MenuWindow()
        {
            this.InitializeComponent();

            this.SetBackground();
        }

        private void SetBackground()
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.StreamSource = Assembly.GetExecutingAssembly().GetManifestResourceStream("DrunkenRush.GameControl.Resources.menu_art.png");
            bmp.EndInit();

            this.Background = new ImageBrush(bmp);
        }

        private void StartGameClick(object sender, RoutedEventArgs e)
        {
            NameFormWindow nameWindow = new NameFormWindow();
            nameWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            nameWindow.Left = this.Left + ((this.Width - this.ActualWidth) / 2);
            nameWindow.Top = this.Top + ((this.Height - this.ActualHeight) / 2);

            if (nameWindow.ShowDialog() == true)
            {
                MainWindow mainWin = new MainWindow(nameWindow.SelectedPlayerName);
                mainWin.WindowStartupLocation = WindowStartupLocation.Manual;
                mainWin.Left = this.Left + ((this.Width - this.ActualWidth) / 2);
                mainWin.Top = this.Top + ((this.Height - this.ActualHeight) / 2);

                mainWin.Closing += (s, arg) =>
                {
                    this.Show();
                };

                this.Hide();
                mainWin.Show();
            }
        }

        private void OpenScoreBoardClick(object sender, RoutedEventArgs e)
        {
            ScoresWindow scoresWindow = new ScoresWindow();
            scoresWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            scoresWindow.Show();
        }

        private void LoadGameClick(object sender, RoutedEventArgs e)
        {
            SavedGamesWindow savedGamesWindow = new SavedGamesWindow();
            savedGamesWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            savedGamesWindow.Show();
        }
    }
}
