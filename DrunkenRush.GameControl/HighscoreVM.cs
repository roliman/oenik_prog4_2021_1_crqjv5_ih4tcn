﻿// <copyright file="HighscoreVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Highscores VM.
    /// </summary>
    public class HighscoreVM : ObservableObject
    {
        /// <summary>
        /// name field.
        /// </summary>
        private string name;

        /// <summary>
        /// score field.
        /// </summary>
        private int score;

        /// <summary>
        /// Initializes a new instance of the <see cref="HighscoreVM"/> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="score">score.</param>
        public HighscoreVM(string name, int score)
        {
            this.Name = name;
            this.Score = score;
        }

        /// <summary>
        /// Gets or Sets name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or Sets score.
        /// </summary>
        public int Score
        {
            get { return this.score; }
            set { this.Set(ref this.score, value); }
        }

        /// <summary>
        /// To string method.
        /// </summary>
        /// <returns>string.</returns>
        public override string ToString()
        {
            return string.Format("{0} - {1}", this.Name, this.Score);
        }
    }
}
