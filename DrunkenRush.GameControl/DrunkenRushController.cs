﻿// <copyright file="DrunkenRushController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    using DrunkenRush.GameLogic;
    using DrunkenRush.GameModel;
    using DrunkenRush.GameRenderer;
    using DrunkenRush.Repository;

    /// <summary>
    /// Controller class.
    /// </summary>
    public class DrunkenRushController : FrameworkElement
    {
        private IGameModel model;
        private IStorageRepository repository;
        private ISaveGameRepository saveGameRepository;
        private IGameLogic logic;
        private DrunkenRushRenderer renderer;
        private DispatcherTimer mainTimer;
        private Window window;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrunkenRushController"/> class.
        /// </summary>
        public DrunkenRushController()
        {
            this.Loaded += this.DrunkenRushController_Loaded;
        }

        /// <inheritdoc/>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (this.renderer != null)
            {
                this.renderer.BuildDisplay(drawingContext);
            }
        }

        private void DrunkenRushController_Loaded(object sender, RoutedEventArgs e)
        {
            this.window = Window.GetWindow(this);

            string pname = (this.window as MainWindow).PlayerName;
            int score = (this.window as MainWindow).Score;
            this.model = new DrunkenRushModel(pname, score, this.ActualWidth, this.ActualHeight);
            this.repository = new Highscore();
            this.saveGameRepository = new SaveGame();
            this.logic = new DrunkenRushLogic(this.model, this.repository, "startingmap.txt");
            this.renderer = new DrunkenRushRenderer(this.model);

            if (this.window != null)
            {
                this.window.KeyDown += this.Window_KeyDown;
                this.mainTimer = new DispatcherTimer();
                this.mainTimer.Interval += TimeSpan.FromMilliseconds(50);
                this.mainTimer.Tick += this.MainTimer_Tick;
                this.mainTimer.Start();
            }

            (this.window as MainWindow).saveGame += this.SaveActualGame;
            this.logic.GameOver += (this.window as MainWindow).GameOverEvent;
            this.repository.NewHighscore += (this.window as MainWindow).NewHighscoreEvent;

            this.InvalidateVisual();
        }

        private void SaveActualGame(object sender, EventArgs e)
        {
            this.saveGameRepository.Save(this.model.Score, this.model.PlayerName);
            this.mainTimer.Stop();
        }

        private void MainTimer_Tick(object sender, EventArgs e)
        {
            this.logic.OneTick();

            this.InvalidateVisual();
        }

        private void Window_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            int h = IsometricTile.TileHeight;
            int w = IsometricTile.TileWidth;

            if (this.model.Player.IsAlive)
            {
                this.model.KeyDownWatch.Restart();

                switch (e.Key)
                {
                    case Key.Up: this.logic.ForwardMove(); break;
                    case Key.Left: this.logic.MovePlayer(-w / 2, -h / 2); break;
                    case Key.Right: this.logic.MovePlayer(w / 2, h / 2); break;
                }
            }

            this.InvalidateVisual();
        }
    }
}