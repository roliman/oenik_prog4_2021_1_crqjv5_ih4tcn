﻿// <copyright file="NameFormWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for NameFormWindow.xaml.
    /// </summary>
    public partial class NameFormWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NameFormWindow"/> class.
        /// </summary>
        public NameFormWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets selected player name.
        /// </summary>
        public string SelectedPlayerName { get; private set; }

        private void StartClick(object sender, RoutedEventArgs e)
        {
            this.SelectedPlayerName = this.textbox_name.Text;
            this.DialogResult = true;
            this.Close();
        }
    }
}
