﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using DrunkenRush.Repository;

    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Eventhandler.
        /// </summary>
        public EventHandler saveGame;

        private ISaveGameRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        public MainWindow(string playerName)
        {
            this.PlayerName = playerName;
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="playerName">Name of the player.</param>
        /// <param name="score">Score.</param>
        public MainWindow(string playerName, int score)
        {
            this.Score = score;
            this.PlayerName = playerName;
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        /// <param name="repository">repository.</param>
        public MainWindow(ISaveGameRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Gets name of the player.
        /// </summary>
        public string PlayerName { get; }

        /// <summary>
        /// Gets score.
        /// </summary>
        public int Score { get; }

        /// <summary>
        /// Game over event.
        /// </summary>
        /// <param name="sender">sender.</param>
        /// <param name="e">args.</param>
        public void GameOverEvent(object sender, EventArgs e)
        {
            MessageBox.Show("GAME OVER");
            this.Close();
        }

        /// <summary>
        /// New highscore has been set event.
        /// </summary>
        /// <param name="sender">sender.</param>
        /// <param name="e">args.</param>
        public void NewHighscoreEvent(object sender, EventArgs e)
        {
            MessageBox.Show("CONGRATULATION: YOU SET A NEW HIGHSCORE!");
            this.Close();
        }

        /// <summary>
        /// Saves the game.
        /// </summary>
        /// <param name="sender">sender.</param>
        /// <param name="e">args.</param>
        private void SaveGameClick(object sender, RoutedEventArgs e)
        {
            this.saveGame?.Invoke(this, EventArgs.Empty);
            this.Close();
        }
    }
}
