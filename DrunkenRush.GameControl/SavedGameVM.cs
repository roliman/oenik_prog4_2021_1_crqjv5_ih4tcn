﻿// <copyright file="SavedGameVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using DrunkenRush.Repository;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// View model for saved games.
    /// </summary>
    public class SavedGameVM : ViewModelBase
    {
        private ISaveGameRepository repository;
        private ObservableCollection<HighscoreVM> saves;
        private HighscoreVM selectedSave;

        /// <summary>
        /// Initializes a new instance of the <see cref="SavedGameVM"/> class.
        /// </summary>
        /// <param name="repository">Repository.</param>
        public SavedGameVM(ISaveGameRepository repository)
        {
            this.repository = repository;
            this.LoadSaves();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SavedGameVM"/> class.
        /// </summary>
        public SavedGameVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<ISaveGameRepository>())
        {
        }

        /// <summary>
        /// Gets or sets saves.
        /// </summary>
        public ObservableCollection<HighscoreVM> Saves
        {
            get { return this.saves; }
            set { this.Set(ref this.saves, value); }
        }

        /// <summary>
        /// Gets or sets selected save.
        /// </summary>
        public HighscoreVM SelectedSave
        {
            get { return this.selectedSave; }
            set { this.Set(ref this.selectedSave, value); }
        }

        /// <summary>
        /// Deletes the selected save.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="score">score.</param>
        public void DeleteSelectedSave(string name, int score)
        {
            this.repository.DeleteSave(name, score);
        }

        private void LoadSaves()
        {
            this.Saves = new ObservableCollection<HighscoreVM>();

            foreach (var item in this.repository.Restore())
            {
                if (!string.IsNullOrEmpty(item))
                {
                    string[] temp = item.Split('\t');
                    this.Saves.Add(new HighscoreVM(temp[0], int.Parse(temp[1])));
                }
            }
        }
    }
}
