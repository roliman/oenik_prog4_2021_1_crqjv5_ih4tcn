﻿// <copyright file="ScoresWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameControl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for ScoresWindow.xaml.
    /// </summary>
    public partial class ScoresWindow : Window
    {
        /// <summary>
        /// ViewModel.
        /// </summary>
        private ScoresVM vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScoresWindow"/> class.
        /// </summary>
        public ScoresWindow()
        {
            this.InitializeComponent();
            this.vm = this.FindResource("VM") as ScoresVM;
        }
    }
}
