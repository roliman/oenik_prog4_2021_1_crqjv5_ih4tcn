﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA2109:Review visible event handlers", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameControl.DrunkenRushController.GameOverEvent(System.Object,System.EventArgs)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant", Justification = "There are places where unsigned values are used, which is considered not Cls compliant.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameControl.ScoresVM.LoadScores")]
[assembly: SuppressMessage("Security", "CA2109:Review visible event handlers", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameControl.MainWindow.GameOverEvent(System.Object,System.EventArgs)")]
[assembly: SuppressMessage("Security", "CA2109:Review visible event handlers", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameControl.MainWindow.NewHighscoreEvent(System.Object,System.EventArgs)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameControl.SavedGameVM.LoadSaves")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "<Pending>", Scope = "member", Target = "~P:DrunkenRush.GameControl.SavedGameVM.Saves")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameControl.HighscoreVM.ToString~System.String")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "<Pending>", Scope = "member", Target = "~F:DrunkenRush.GameControl.MainWindow.saveGame")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1307:Accessible fields should begin with upper-case letter", Justification = "<Pending>", Scope = "member", Target = "~F:DrunkenRush.GameControl.MainWindow.saveGame")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameControl.SavedGamesWindow.ListBox_MouseDoubleClick(System.Object,System.Windows.Input.MouseButtonEventArgs)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "<Pending>", Scope = "member", Target = "~F:DrunkenRush.GameControl.MainWindow.saveGame")]
