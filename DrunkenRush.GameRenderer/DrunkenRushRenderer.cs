﻿// <copyright file="DrunkenRushRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameRenderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using DrunkenRush.GameModel;
    using DrunkenRush.GameModel.DrawableTiles;
    using DrunkenRush.GameModel.Obstacles;

    /// <summary>
    /// This class responsible for draw geometry to screen.
    /// </summary>
    public class DrunkenRushRenderer
    {
        private IGameModel model;

        private readonly IsometricTile[,] mapToRender;

        private FormattedText formattedText;
        private FormattedText formattedText2;
        private Typeface font = new Typeface("Showcard Gothic");
        private Point textLocation = new Point(10, 10);
        private Point textLocation2 = new Point(270, 10);

        /// <summary>
        /// Initializes a new instance of the <see cref="DrunkenRushRenderer"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public DrunkenRushRenderer(IGameModel model)
        {
            this.model = model;
            this.mapToRender = this.model.Map;
        }

        /// <summary>
        /// Builds up the map.
        /// </summary>
        /// <param name="ctx">context.</param>
        public void BuildDisplay(DrawingContext ctx)
        {
            if (ctx != null)
            {
                this.DrawIsometricGrid(ctx);
                this.DrawObjects(ctx);
                this.DrawPlayer(ctx);
                this.DrawPlayerCoordinates(ctx);
            }
        }

        /// <summary>
        /// This methods iterates through the list to be drawn.
        /// </summary>
        /// <param name="ctx">context.</param>
        private void DrawIsometricGrid(DrawingContext ctx)
        {
            int height = this.model.Map.GetLength(0);
            int width = this.model.Map.GetLength(1);

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    DrawingGroup drawingGroup = new DrawingGroup();

                    GeometryDrawing isotile = new GeometryDrawing(this.mapToRender[i, j].Brush, new Pen(Brushes.Black, 0.2), this.mapToRender[i, j].GetGeometry());

                    drawingGroup.Children.Add(isotile);

                    ctx.DrawDrawing(drawingGroup);
                }
            }
        }

        private void DrawObjects(DrawingContext ctx)
        {
            for (int i = this.model.Objects.Count - 1; i > -1; i--)
            {
                var item = this.model.Objects[i];

                if (item is Car)
                {
                    ctx.DrawGeometry((item as Car).Brush, null, item.RealArea);
                }
                else if (item is Log)
                {
                    ctx.DrawGeometry(item.Ib, null, item.RealArea);
                }
                else
                {
                    ctx.DrawGeometry(item.Ib, null, item.RealArea);
                }
            }
        }

        private void DrawPlayer(DrawingContext ctx)
        {
            ctx.DrawGeometry(this.model.Player.Ib, null, this.model.Player.RealArea);
        }

        private void DrawPlayerCoordinates(DrawingContext ctx)
        {
            this.formattedText = new FormattedText(
                string.Format("{0} - seconds", 8 - (this.model.KeyDownWatch.ElapsedMilliseconds / 1000)),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.font,
                22,
                Brushes.Red,
                1.0);

            this.formattedText2 = new FormattedText(
                string.Format("Score: {0}", this.model.Score),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                this.font,
                22,
                Brushes.Black,
                1.0);

            ctx.DrawText(this.formattedText, this.textLocation);
            ctx.DrawText(this.formattedText2, this.textLocation2);
        }
    }
}
