﻿// <copyright file="SaveGame.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Runtime.Serialization.Formatters.Soap;
    using System.Text;
    using System.Threading.Tasks;
    using DrunkenRush.GameModel;

    /// <summary>
    /// Save game repository.
    /// </summary>
    public class SaveGame : ISaveGameRepository
    {
        private readonly string path = @"..\..\..\saves.txt";
        private StreamWriter sw;

        /// <inheritdoc/>
        public void Save(int score, string name)
        {
            string playername = string.IsNullOrEmpty(name) ? "Anonymous" : name;

            this.sw = new StreamWriter(this.path, true);

            this.sw.WriteLine(playername + '\t' + score);

            this.sw.Close();
        }

        /// <inheritdoc/>
        public string[] Restore()
        {
            StreamReader sr = new StreamReader(this.path);
            string[] saves = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');

            sr.Close();

            return saves;
        }

        /// <inheritdoc/>
        public void DeleteSave(string name, int score)
        {
            string linetodelete = name + '\t' + score;

            StreamReader sr = new StreamReader(this.path);
            string[] lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');
            sr.Close();

            this.sw = new StreamWriter(this.path);

            foreach (string line in lines)
            {
                if (line != linetodelete)
                {
                    this.sw.WriteLine(line);
                }
            }

            this.sw.Close();
        }
    }
}
