﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant", Justification = "There are places where unsigned values are used, which is considered not Cls compliant.")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.Repository.Highscore.StoreHighScore(System.String,System.Int32)")]
[assembly: SuppressMessage("Design", "CA1001:Types that own disposable fields should be disposable", Justification = "this", Scope = "type", Target = "~T:DrunkenRush.Repository.SaveGame")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.Repository.SaveGame.Restore~System.String[]")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.Repository.SaveGame.Restore~System.String[]")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.Repository.SaveGame.DeleteSave(System.String,System.Int32)")]
