﻿// <copyright file="IStorageRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Storage repository.
    /// </summary>
    public interface IStorageRepository
    {
        /// <summary>
        /// Gets or sets newhighscore event.
        /// </summary>
        public EventHandler NewHighscore { get; set; }

        /// <summary>
        /// Stores a new highscore.
        /// </summary>
        /// <param name="name">player name.</param>
        /// <param name="score">score to store.</param>
        void StoreHighScore(string name, int score);

        /// <summary>
        /// Returns highscore.
        /// </summary>
        /// <returns>Highscore.</returns>
        string GetHighScore();

        /// <summary>
        /// Return all scores.
        /// </summary>
        /// <returns>XElements.</returns>
        IEnumerable<XElement> GetScores();
    }
}
