﻿// <copyright file="ISaveGameRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrunkenRush.GameModel;

    /// <summary>
    /// Interface for save the game function.
    /// </summary>
    public interface ISaveGameRepository
    {
        /// <summary>
        /// Save the game model.
        /// </summary>
        /// <param name="score">player score.</param>
        /// <param name="name">player name.</param>
        void Save(int score, string name);

        /// <summary>
        /// Restores the game model.
        /// </summary>
        /// <returns>game model.</returns>
        string[] Restore();

        /// <summary>
        /// Deletes a save when it were loaded.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="score">score.</param>
        void DeleteSave(string name, int score);
    }
}
