﻿// <copyright file="Highscore.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;

    /// <summary>
    /// Stores highscore.
    /// </summary>
    public sealed class Highscore : IStorageRepository
    {
        /// <inheritdoc/>
        public EventHandler NewHighscore { get; set; }

        /// <inheritdoc/>
        public void StoreHighScore(string name, int score)
        {
            XDocument xDocument;

            try
            {
                xDocument = XDocument.Load("highscores.xml");
            }
            catch (System.IO.FileNotFoundException)
            {
                XDocument xdoc = new XDocument(
                    new XElement(
                    "highscore",
                    new XElement(
                    "player",
                    new XElement("name", name),
                    new XElement("score", score))));

                xdoc.Save("highscores.xml");
                this.NewHighscore?.Invoke(this, EventArgs.Empty);
                return;
            }

            var gamer = from player in xDocument.Root.Descendants("player")
                        where player.Element("name").Value == name
                        select player;

            if (!gamer.Any() || gamer.Single().Element("name").Value != name)
            {
                xDocument.Root.Add(new XElement(
                    "player",
                    new XElement("name", name),
                    new XElement("score", score)));

                this.NewHighscore?.Invoke(this, EventArgs.Empty);
            }
            else
            {
                if (int.Parse(gamer.Single().Element("score").Value) < score)
                {
                    gamer.Single().Element("score").Value = score.ToString();

                    this.NewHighscore?.Invoke(this, EventArgs.Empty);
                }
            }

            xDocument.Save("highscores.xml");
        }

        /// <inheritdoc/>
        public string GetHighScore()
        {
            XDocument xDocument;

            try
            {
                xDocument = XDocument.Load("highscores.xml");
            }
            catch (System.IO.FileNotFoundException)
            {
                return null;
            }

            var highscore = from x in xDocument.Root.Descendants("player")
                            select x.Element("score").Value;

            return highscore.SingleOrDefault();
        }

        /// <inheritdoc/>
        IEnumerable<XElement> IStorageRepository.GetScores()
        {
            XDocument xdoc;

            try
            {
                xdoc = XDocument.Load("highscores.xml");
            }
            catch (System.IO.FileNotFoundException)
            {
                return null;
            }

            return xdoc.Root.Elements();
        }
    }
}
