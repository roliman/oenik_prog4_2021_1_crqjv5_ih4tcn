﻿// <copyright file="DrunkenRushModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Threading;
    using DrunkenRush.GameModel.DrawableTiles;
    using DrunkenRush.GameModel.Obstacles;

    /// <summary>
    /// Main model for game DrunkenRush.
    /// </summary>
    public class DrunkenRushModel : IGameModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DrunkenRushModel"/> class.
        /// </summary>
        /// <param name="playerName">Player name.</param>
        /// <param name="score">Score.</param>
        /// <param name="w">game width.</param>
        /// <param name="h">game height.</param>
        public DrunkenRushModel(string playerName, int score, double w, double h)
        {
            this.KeyDownWatch = new Stopwatch();
            this.GameWidth = w;
            this.GameHeight = h;
            this.Objects = new List<GameItem>();
            this.Score = score;
            this.PlayerName = playerName;
            this.Player = new Player(160, 340);
            this.Objects.Add(new Car(270, 170, Direction.Up));
            this.Objects.Add(new Log(520, 160));
            this.Objects.Add(new Log(560, 140));
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        public double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets keyDownWatch.
        /// </summary>
        public Stopwatch KeyDownWatch { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        public double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets player model.
        /// </summary>
        public Player Player { get; set; }

        /// <summary>
        /// Gets or sets objects.
        /// </summary>
        public List<GameItem> Objects { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// </summary>
        public IsometricTile[,] Map { get; set; }

        /// <summary>
        /// Gets or sets score.
        /// </summary>
        public int Score { get; set; }

        /// <inheritdoc/>
        public string PlayerName { get; set; }
    }
}
