﻿// <copyright file="Car.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Car class.
    /// </summary>
    public class Car : GameItem
    {
        /// <summary>
        /// Height of the car.
        /// </summary>
        public const int CarHeight = 80;

        /// <summary>
        /// Width of the car.
        /// </summary>
        public const int CarWidth = 160;

        private static Random rnd = new Random();

        private Rect carHitBox;

        private ImageBrush brush;

        /// <summary>
        /// Initializes a new instance of the <see cref="Car"/> class.
        /// </summary>
        /// <param name="cx">CX.</param>
        /// <param name="cy">CY.</param>
        /// <param name="direction">Direction of the car.</param>
        public Car(double cx, double cy, Direction direction)
        {
            this.CX = cx;
            this.CY = cy;
            this.Speed = 5;
            this.RotX = CarWidth / 2;
            this.RotY = CarHeight / 2;
            this.rotDegree = 25;
            this.IsVisible = true;
            this.Direction = direction;
            this.carHitBox = new Rect(this.CX - this.CX, this.CY - this.CY, CarWidth, CarHeight);
            Geometry g = new RectangleGeometry(this.carHitBox);
            this.area = g;
        }

        /// <summary>
        /// Gets ImageBrush of cars.
        /// </summary>
        public ImageBrush Brush
        {
            get
            {
                if (this.brush == null)
                {
                    this.brush = new ImageBrush();
                    if (this.Direction == Direction.Up)
                    {
                        switch (rnd.Next(1, 4))
                        {
                            case 1: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\p_car_u.png", UriKind.Relative)); break;
                            case 2: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\blue_car_u.png", UriKind.Relative)); break;
                            case 3: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\b_car_u.png", UriKind.Relative)); break;
                            default:
                                break;
                        }
                    }
                    else if (this.Direction == Direction.Down)
                    {
                        switch (rnd.Next(1, 4))
                        {
                            case 1: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\p_car_d.png", UriKind.Relative)); break;
                            case 2: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\blue_car_d.png", UriKind.Relative)); break;
                            case 3: this.brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\b_car_d.png", UriKind.Relative)); break;
                            default:
                                break;
                        }
                    }
                }

                return this.brush;
            }
        }

        /// <inheritdoc/>
        public override int GetObjectHeight
        {
            get { return CarHeight; }
        }
    }
}
