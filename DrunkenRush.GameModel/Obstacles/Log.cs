﻿// <copyright file="Log.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class of the log.
    /// </summary>
    public class Log : GameItem
    {
        /// <summary>
        ///  Height of the log.
        /// </summary>
        public const int LogHeight = 40;

        /// <summary>
        /// Width of the log.
        /// </summary>
        public const int LogWidth = 60;

        /// <summary>
        /// Basic speed of the log.
        /// </summary>
        public const int LogSpeed = 20;

        /// <summary>
        /// Hibtox of the log.
        /// </summary>
        private Rect logHitBox;

        /// <summary>
        /// Initializes a new instance of the <see cref="Log"/> class.
        /// </summary>
        /// <param name="cx">CX.</param>
        /// <param name="cy">CY.</param>
        public Log(double cx, double cy)
        {
            this.CX = cx;
            this.CY = cy;
            this.Speed = LogSpeed;
            this.RotX = LogWidth / 2;
            this.RotY = LogHeight / 2;
            this.Ib = new ImageBrush();
            this.Ib.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\log.png", UriKind.Relative));
            this.rotDegree = 30;
            this.logHitBox = new Rect(this.CX - this.CX, this.CY - this.CY, LogWidth, LogHeight);
            this.IsVisible = true;

            Geometry g = new RectangleGeometry(this.logHitBox);
            this.area = g;
        }

        /// <inheritdoc/>
        public override int GetObjectHeight
        {
            get { return LogHeight; }
        }
    }
}
