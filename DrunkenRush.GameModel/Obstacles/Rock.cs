﻿// <copyright file="Rock.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel.Obstacles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Rock object.
    /// </summary>
    public sealed class Rock : GameItem
    {
        /// <summary>
        /// Object width.
        /// </summary>
        public const int RectWidth = 80;

        /// <summary>
        /// Object height.
        /// </summary>
        public const int RectHeight = 40;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rock"/> class.
        /// </summary>
        /// <param name="x">x param.</param>
        /// <param name="y">y param.</param>
        public Rock(double x, double y)
        {
            this.CX = x;
            this.CY = y;

            this.Ib = new ImageBrush();
            this.Ib.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\rock.png", UriKind.Relative));

            this.Direction = Direction.Down;
            this.IsVisible = true;

            GeometryGroup g = new GeometryGroup();
            Rect rect = new Rect(this.CX - this.CX, this.CY - this.CY, RectWidth, RectHeight);
            g.Children.Add(new RectangleGeometry(rect));
            this.area = g;
        }

        /// <inheritdoc/>
        public override int GetObjectHeight
        {
            get { return RectHeight; }
        }
    }
}
