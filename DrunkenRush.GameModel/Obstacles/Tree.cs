﻿// <copyright file="Tree.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Tree object.
    /// </summary>
    public sealed class Tree : GameItem
    {
        /// <summary>
        /// Object width.
        /// </summary>
        public const int RectWidth = 80;

        /// <summary>
        /// Object height.
        /// </summary>
        public const int RectHeight = 40;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tree"/> class.
        /// </summary>
        /// <param name="x">x pos.</param>
        /// <param name="y">y pos.</param>
        public Tree(double x, double y)
        {
            this.CX = x;
            this.CY = y;

            this.Ib = new ImageBrush();
            this.Ib.ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\bush.png", UriKind.Relative));

            this.Direction = Direction.Down;
            this.IsVisible = true;

            GeometryGroup g = new GeometryGroup();
            Rect rect = new Rect(this.CX - this.CX, this.CY - this.CY, RectWidth, RectHeight); // Push objects back to original place because of TranslateTransform.
            g.Children.Add(new RectangleGeometry(rect));
            this.area = g;
        }

        /// <inheritdoc/>
        public override int GetObjectHeight
        {
            get { return RectHeight; }
        }
    }
}
