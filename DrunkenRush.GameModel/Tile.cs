﻿// <copyright file="Tile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;

    /// <summary>
    /// The basic map tile for every object.
    /// </summary>
    public abstract class Tile
    {
        private Rect area;

        /// <summary>
        /// Initializes a new instance of the <see cref="Tile"/> class.
        /// </summary>
        /// <param name="x">tile x coordinate.</param>
        /// <param name="y">tile y coordinate.</param>
        /// <param name="w">tile width.</param>
        /// <param name="h">tile height.</param>
        protected Tile(double x, double y, double w, double h)
        {
            this.area = new Rect(x, y, w, h);
        }

        /// <summary>
        /// Gets area.
        /// </summary>
        public Rect Area
        {
            get { return this.area; }
        }

        /// <summary>
        /// Sets the coordinates.
        /// </summary>
        /// <param name="x">new x coord.</param>
        /// <param name="y">new y coord.</param>
        public void SetXY(double x, double y)
        {
            this.area.X = x;
            this.area.Y = y;
        }
    }
}
