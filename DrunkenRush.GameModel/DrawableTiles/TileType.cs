﻿// <copyright file="TileType.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Enum for tile types.
    /// </summary>
    public enum TileType
    {
        /// <summary>
        /// Grass type.
        /// </summary>
        Grass,

        /// <summary>
        /// Asphalt type.
        /// </summary>
        Asphalt,

        /// <summary>
        /// Water type.
        /// </summary>
        Water,

        /// <summary>
        /// Dirt type.
        /// </summary>
        Dirt,
    }
}
