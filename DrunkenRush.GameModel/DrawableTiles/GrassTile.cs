﻿// <copyright file="GrassTile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Grass tile.
    /// </summary>
    public static class GrassTile
    {
        private static ImageBrush brush;

        /// <summary>
        /// Gets tile type.
        /// </summary>
        public static TileType Type
        {
            get
            {
                return TileType.Grass;
            }
        }

        /// <summary>
        /// Gets brush.
        /// </summary>
        public static ImageBrush Brush
        {
            get
            {
                if (brush == null)
                {
                    brush = new ImageBrush();
                    brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Tiles\grassTile.png", UriKind.Relative));
                }

                return brush;
            }
        }
    }
}
