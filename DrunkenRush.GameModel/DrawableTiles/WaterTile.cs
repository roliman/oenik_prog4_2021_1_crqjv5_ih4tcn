﻿// <copyright file="WaterTile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel.DrawableTiles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Water tile.
    /// </summary>
    public static class WaterTile
    {
        private static ImageBrush brush;

        /// <summary>
        /// Gets tile type.
        /// </summary>
        public static TileType Type
        {
            get
            {
                return TileType.Water;
            }
        }

        /// <summary>
        /// Gets brush.
        /// </summary>
        public static ImageBrush Brush
        {
            get
            {
                if (brush == null)
                {
                    brush = new ImageBrush();
                    brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Tiles\waterTile.png", UriKind.Relative));
                }

                return brush;
            }
        }
    }
}
