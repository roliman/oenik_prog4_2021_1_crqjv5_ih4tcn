﻿// <copyright file="IsometricTile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Numerics;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Main game element.
    /// </summary>
    [Serializable]
    public sealed class IsometricTile : Tile
    {
        /// <summary>
        /// Tile witdh.
        /// </summary>
        public const int TileWidth = 80;  // 80 pixel wide 2:1 ratio

        /// <summary>
        /// Tile height.
        /// </summary>
        public const int TileHeight = 40; // 40 pixel wide 2:1 ratio

        /// <summary>
        /// Initializes a new instance of the <see cref="IsometricTile"/> class.
        /// </summary>
        /// <param name="x">the tile x coordinate.</param>
        /// <param name="y">the tile y coordinate.</param>
        /// <param name="type">the type of the tile.</param>
        /// <param name="ib">imagebrush to fill the tile.</param>
        public IsometricTile(double x, double y, TileType type, ImageBrush ib)
            : base(x, y, TileWidth, TileHeight)
        {
            this.Type = type;
            this.Brush = ib;
        }

        /// <summary>
        /// Gets or sets tile brush.
        /// </summary>
        public ImageBrush Brush { get; set; }

        /// <summary>
        /// Gets or sets tile type.
        /// </summary>
        public TileType Type { get; set; }

        /// <summary>
        /// Isometric tile.
        /// </summary>
        /// <returns>Geometry item.</returns>
        public Geometry GetGeometry()
        {
            List<Point> points = new List<Point>();

            points.Add(new Point(this.Area.X + (TileWidth / 2), this.Area.Y));
            points.Add(new Point(this.Area.X + TileWidth, this.Area.Y + (TileHeight / 2)));
            points.Add(new Point(this.Area.X + (TileWidth / 2), this.Area.Y + TileHeight));
            points.Add(new Point(this.Area.X, this.Area.Y + (TileHeight / 2)));

            StreamGeometry geo = new StreamGeometry();
            using (StreamGeometryContext ctx = geo.Open())
            {
                ctx.BeginFigure(points[0], true, true);

                ctx.PolyLineTo(points, true, true);

                return geo;
            }
        }
    }
}
