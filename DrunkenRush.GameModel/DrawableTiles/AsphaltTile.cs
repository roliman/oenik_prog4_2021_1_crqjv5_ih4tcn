﻿// <copyright file="AsphaltTile.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel.DrawableTiles
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Asphalt tile element.
    /// </summary>
    public static class AsphaltTile
    {
        private static ImageBrush brush;

        /// <summary>
        /// Gets tile type.
        /// </summary>
        public static TileType Type
        {
            get
            {
                return TileType.Asphalt;
            }
        }

        /// <summary>
        /// Gets brush.
        /// </summary>
        public static ImageBrush Brush
        {
            get
            {
                if (brush == null)
                {
                    brush = new ImageBrush();
                    brush.ImageSource = new BitmapImage(new Uri(@"..\..\..\Tiles\asphaltTile.png", UriKind.Relative));
                }

                return brush;
            }
        }
    }
}
