﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Gameitem class.
    /// </summary>
    [Serializable]
    public abstract class GameItem
    {
        /// <summary>
        /// Area.
        /// </summary>
        private protected Geometry area;

        /// <summary>
        /// Rotation degree.
        /// </summary>
        private protected double rotDegree;

        /// <summary>
        /// Gets or sets x coordinate.
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets y coordinate.
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets x coordinate used for rotation.
        /// </summary>
        public double RotX { get; set; }

        /// <summary>
        /// Gets or sets y coordinate used for rotation.
        /// </summary>
        public double RotY { get; set; }

        /// <summary>
        /// Gets or sets speed.
        /// </summary>
        public double Speed { get; set; }

        /// <summary>
        /// Gets or sets direction.
        /// </summary>
        public Direction Direction { get; set; }

        /// <summary>
        /// Gets or sets imagebrush.
        /// </summary>
        public ImageBrush Ib { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the item is visible or not.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets object height.
        /// </summary>
        public abstract int GetObjectHeight { get; }

        /// <summary>
        /// Gets the realarea of a gameitem.
        /// </summary>
        public Geometry RealArea
        {
            get
            {
                TransformGroup tr = new TransformGroup();
                tr.Children.Add(new RotateTransform(this.rotDegree, this.RotX, this.RotY));
                tr.Children.Add(new TranslateTransform(this.CX, this.CY));
                this.area.Transform = tr;
                return this.area.GetFlattenedPathGeometry();
            }
        }

        /// <summary>
        /// Method to detect collision.
        /// </summary>
        /// <param name="other">Other object.</param>
        /// <returns>Whether there is collision or not.</returns>
        public bool IsCollision(GameItem other)
        {
            return (Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 2800) && this.RealArea.FillContains((other as Player).PlayerPoint);
        }
    }
}
