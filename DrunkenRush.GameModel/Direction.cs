﻿// <copyright file="Direction.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Direction enum.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// Direction up.
        /// </summary>
        Up,

        /// <summary>
        /// Direction down.
        /// </summary>
        Down,
    }
}
