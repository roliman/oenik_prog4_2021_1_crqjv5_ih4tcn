﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface for IGameModel.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        double GameWidth { get; set; }

        /// <summary>
        /// Gets or sets keyDownWatch.
        /// </summary>
        Stopwatch KeyDownWatch { get; set; }

        /// <summary>
        /// Gets or sets the height.
        /// </summary>
        double GameHeight { get; set; }

        /// <summary>
        /// Gets or sets player model.
        /// </summary>
        Player Player { get; set; }

        /// <summary>
        /// Gets or sets objects.
        /// </summary>
        List<GameItem> Objects { get; set; }

        /// <summary>
        /// Gets or sets the map.
        /// Gets or ets the map.
        /// </summary>
        IsometricTile[,] Map { get; set; }

        /// <summary>
        /// Gets or sets score.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets or Sets playername.
        /// </summary>
        public string PlayerName { get; set; }
    }
}
