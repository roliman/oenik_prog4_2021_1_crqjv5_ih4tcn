﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class representing Player.
    /// </summary>
    [Serializable]
    public class Player : GameItem
    {
        /// <summary>
        /// Height of the player.
        /// </summary>
        public const int PlayerTileHeight = 80;

        /// <summary>
        /// Width of the player.
        /// </summary>
        public const int PlayerTileWidth = 80;

        private Rect playerHitBox;

        private Point playerPoint;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="cx">CX.</param>
        /// <param name="cy">CY.</param>
        public Player(double cx, double cy)
        {
            this.IsAlive = true;

            this.CX = cx;
            this.CY = cy;
            this.Ib = new ImageBrush
            {
                ImageSource = new BitmapImage(new Uri(@"..\..\..\Objects\figura.png", UriKind.Relative)),
            };
            this.playerHitBox = new Rect(this.CX - this.CX, this.CY - this.CY, PlayerTileHeight, PlayerTileWidth);

            Geometry g = new RectangleGeometry(this.playerHitBox);
            this.area = g;
            double pointX = this.CX + (PlayerTileWidth / 2);
            double pointY = this.CY + ((PlayerTileHeight / 4) * 3) + 5;
            this.playerPoint = new Point(pointX, pointY);
        }

        /// <summary>
        /// Gets or sets a value indicating whether player is alive or not.
        /// </summary>
        public bool IsAlive { get; set; }

        /// <summary>
        /// Gets IB.
        /// </summary>
        public new ImageBrush Ib { get; private set; }

        /// <summary>
        /// Gets approximately where the player stands.
        /// </summary>
        public Point PlayerPoint
        {
            get
            {
                return this.playerPoint;
            }
        }

        /// <summary>
        /// Gets playerhitbox.
        /// </summary>
        public Rect PlayerHitBox
        {
            get
            {
                return this.playerHitBox;
            }
        }

        /// <inheritdoc/>
        public override int GetObjectHeight
        {
            get { return PlayerTileHeight; }
        }
    }
}
