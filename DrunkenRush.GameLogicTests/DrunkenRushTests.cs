﻿// <copyright file="DrunkenRushTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameLogicTests
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Threading;
    using DrunkenRush.GameLogic;
    using DrunkenRush.GameModel;
    using DrunkenRush.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class DrunkenRushTests
    {
        private DrunkenRushLogic logic;

        private Mock<IGameModel> model;

        private Mock<IStorageRepository> repo;

        private List<GameItem> objects;

        private Player player;

        /// <summary>
        /// Setup for test methods.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.repo = new Mock<IStorageRepository>();
            this.model = new Mock<IGameModel>();
            this.objects = new List<GameItem>();
            this.objects.Add(new Car(550, 230, Direction.Down));
            this.objects.Add(new Log(180, 300));
            this.player = new Player(160, 340);
            this.repo.Setup(x => x.GetHighScore()).Returns("100");
            this.model.Setup(x => x.GameWidth).Returns(384);
            this.model.Setup(x => x.GameHeight).Returns(461);
            this.model.Setup(x => x.Player).Returns(this.player);
            this.model.Setup(x => x.Objects).Returns(this.objects);

            this.logic = new DrunkenRushLogic(this.model.Object, this.repo.Object);
        }

        /// <summary>
        /// Test that roudns up Y.
        /// </summary>
        [Test]
        public void RoundUpYTest()
        {
            double testY = 253;
            double newY = this.logic.RoundUpY(testY);
            Assert.That(newY == 260);
        }

        /// <summary>
        /// Test that roudns up X.
        /// </summary>
        [Test]
        public void RoundUpXTest()
        {
            double testX = 73;
            double newX = this.logic.RoundUpX(testX);
            Assert.That(newX == 80);
        }

        /// <summary>
        /// Test method to check next step.
        /// </summary>
        [Test]
        public void CheckNextStepTest()
        {
            // w / 2, h / 2)
            int newX = 40;
            int newY = 20;

            bool result = this.logic.CheckNextStep(newX, newY);
            Assert.That(result == true);
        }

        /// <summary>
        /// Test method to store highscore.
        /// </summary>
        [Test]
        public void GameItemDirectionTest()
        {
            List<GameItem> list = new List<GameItem>();
            list.Add(new Log(10, 10));

            this.logic.GameItemDirection(list);

            Assert.That(list[0].Direction == Direction.Down);
        }

        /// <summary>
        /// Test for object movement.
        /// </summary>
        [Test]
        public void MoveObjectsTest()
        {
            var car = this.model.Object.Objects.Find(x => x.CX == 550);
            double carOldX = car.CX;
            double caroldY = car.CY;
            this.logic.MoveObjects(this.model.Object.Objects);

            Assert.That(carOldX > car.CX && caroldY < car.CY);
        }
    }
}
