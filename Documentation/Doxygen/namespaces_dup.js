var namespaces_dup =
[
    [ "DrunkenRush", null, [
      [ "GameLogic", "namespace_drunken_rush_1_1_game_logic.html", null ],
      [ "GameLogicTests", "namespace_drunken_rush_1_1_game_logic_tests.html", null ],
      [ "GameModel", "namespace_drunken_rush_1_1_game_model.html", "namespace_drunken_rush_1_1_game_model" ],
      [ "GameRenderer", "namespace_drunken_rush_1_1_game_renderer.html", null ],
      [ "Repository", "namespace_drunken_rush_1_1_repository.html", null ]
    ] ]
];