var interface_drunken_rush_1_1_game_model_1_1_i_game_model =
[
    [ "GameHeight", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a4e10a87816242963301ae4652538b820", null ],
    [ "GameWidth", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a6ccc2832426a6a65c4e37fe720d57c65", null ],
    [ "KeyDownWatch", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#acab3e3ab20ee53048f9eae0a2c20a529", null ],
    [ "Map", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a70958a3a154f710a75cb8a5b99d0091d", null ],
    [ "Objects", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a3f65d7be7cc56e72f6dba1b768b4ec0f", null ],
    [ "Player", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a7d35acc1e52242eee5b8aff557fc855c", null ],
    [ "PlayerName", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#ac417f20bd3cac34e6bff4a3fa2e3260b", null ],
    [ "Score", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html#a10ca4d8c41eb345b273677652b8ebe63", null ]
];