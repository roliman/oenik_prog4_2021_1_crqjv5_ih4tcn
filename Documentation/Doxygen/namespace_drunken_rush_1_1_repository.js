var namespace_drunken_rush_1_1_repository =
[
    [ "Highscore", "class_drunken_rush_1_1_repository_1_1_highscore.html", "class_drunken_rush_1_1_repository_1_1_highscore" ],
    [ "ISaveGameRepository", "interface_drunken_rush_1_1_repository_1_1_i_save_game_repository.html", "interface_drunken_rush_1_1_repository_1_1_i_save_game_repository" ],
    [ "IStorageRepository", "interface_drunken_rush_1_1_repository_1_1_i_storage_repository.html", "interface_drunken_rush_1_1_repository_1_1_i_storage_repository" ],
    [ "SaveGame", "class_drunken_rush_1_1_repository_1_1_save_game.html", "class_drunken_rush_1_1_repository_1_1_save_game" ]
];