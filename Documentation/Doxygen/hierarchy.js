var hierarchy =
[
    [ "Application", null, [
      [ "DrunkenRush::GameControl::App", "class_drunken_rush_1_1_game_control_1_1_app.html", null ],
      [ "DrunkenRush::GameControl::App", "class_drunken_rush_1_1_game_control_1_1_app.html", null ],
      [ "DrunkenRush::GameControl::App", "class_drunken_rush_1_1_game_control_1_1_app.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.DrawableTiles.AsphaltTile", "class_drunken_rush_1_1_game_model_1_1_drawable_tiles_1_1_asphalt_tile.html", null ],
    [ "DrunkenRush.GameModel.DrawableTiles.DirtTile", "class_drunken_rush_1_1_game_model_1_1_drawable_tiles_1_1_dirt_tile.html", null ],
    [ "DrunkenRush.GameRenderer.DrunkenRushRenderer", "class_drunken_rush_1_1_game_renderer_1_1_drunken_rush_renderer.html", null ],
    [ "DrunkenRush.GameLogicTests.DrunkenRushTests", "class_drunken_rush_1_1_game_logic_tests_1_1_drunken_rush_tests.html", null ],
    [ "FrameworkElement", null, [
      [ "DrunkenRush.GameControl.DrunkenRushController", "class_drunken_rush_1_1_game_control_1_1_drunken_rush_controller.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.GameItem", "class_drunken_rush_1_1_game_model_1_1_game_item.html", [
      [ "DrunkenRush.GameModel.Car", "class_drunken_rush_1_1_game_model_1_1_car.html", null ],
      [ "DrunkenRush.GameModel.Log", "class_drunken_rush_1_1_game_model_1_1_log.html", null ],
      [ "DrunkenRush.GameModel.Obstacles.Rock", "class_drunken_rush_1_1_game_model_1_1_obstacles_1_1_rock.html", null ],
      [ "DrunkenRush.GameModel.Player", "class_drunken_rush_1_1_game_model_1_1_player.html", null ],
      [ "DrunkenRush.GameModel.Tree", "class_drunken_rush_1_1_game_model_1_1_tree.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.GrassTile", "class_drunken_rush_1_1_game_model_1_1_grass_tile.html", null ],
    [ "IComponentConnector", null, [
      [ "DrunkenRush::GameControl::MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
      [ "DrunkenRush::GameControl::MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
      [ "DrunkenRush::GameControl::MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
      [ "DrunkenRush::GameControl::MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
      [ "DrunkenRush::GameControl::NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
      [ "DrunkenRush::GameControl::NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
      [ "DrunkenRush::GameControl::SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
      [ "DrunkenRush::GameControl::SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
      [ "DrunkenRush::GameControl::ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ],
      [ "DrunkenRush::GameControl::ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ]
    ] ],
    [ "DrunkenRush.GameLogic.IGameLogic", "interface_drunken_rush_1_1_game_logic_1_1_i_game_logic.html", [
      [ "DrunkenRush.GameLogic.DrunkenRushLogic", "class_drunken_rush_1_1_game_logic_1_1_drunken_rush_logic.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.IGameModel", "interface_drunken_rush_1_1_game_model_1_1_i_game_model.html", [
      [ "DrunkenRush.GameModel.DrunkenRushModel", "class_drunken_rush_1_1_game_model_1_1_drunken_rush_model.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace::GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "DrunkenRush.Repository.ISaveGameRepository", "interface_drunken_rush_1_1_repository_1_1_i_save_game_repository.html", [
      [ "DrunkenRush.Repository.SaveGame", "class_drunken_rush_1_1_repository_1_1_save_game.html", null ]
    ] ],
    [ "IServiceLocator", null, [
      [ "DrunkenRush.GameControl.MyIoc", "class_drunken_rush_1_1_game_control_1_1_my_ioc.html", null ]
    ] ],
    [ "DrunkenRush.Repository.IStorageRepository", "interface_drunken_rush_1_1_repository_1_1_i_storage_repository.html", [
      [ "DrunkenRush.Repository.Highscore", "class_drunken_rush_1_1_repository_1_1_highscore.html", null ]
    ] ],
    [ "DrunkenRush.GameLogic.MapGenerator", "class_drunken_rush_1_1_game_logic_1_1_map_generator.html", null ],
    [ "ObservableObject", null, [
      [ "DrunkenRush.GameControl.HighscoreVM", "class_drunken_rush_1_1_game_control_1_1_highscore_v_m.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "DrunkenRush.GameControl.MyIoc", "class_drunken_rush_1_1_game_control_1_1_my_ioc.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.Tile", "class_drunken_rush_1_1_game_model_1_1_tile.html", [
      [ "DrunkenRush.GameModel.IsometricTile", "class_drunken_rush_1_1_game_model_1_1_isometric_tile.html", null ]
    ] ],
    [ "ViewModelBase", null, [
      [ "DrunkenRush.GameControl.SavedGameVM", "class_drunken_rush_1_1_game_control_1_1_saved_game_v_m.html", null ],
      [ "DrunkenRush.GameControl.ScoresVM", "class_drunken_rush_1_1_game_control_1_1_scores_v_m.html", null ]
    ] ],
    [ "DrunkenRush.GameModel.DrawableTiles.WaterTile", "class_drunken_rush_1_1_game_model_1_1_drawable_tiles_1_1_water_tile.html", null ],
    [ "Window", null, [
      [ "DrunkenRush::GameControl::MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
      [ "DrunkenRush::GameControl::MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
      [ "DrunkenRush::GameControl::MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
      [ "DrunkenRush::GameControl::MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
      [ "DrunkenRush::GameControl::NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
      [ "DrunkenRush::GameControl::NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
      [ "DrunkenRush::GameControl::SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
      [ "DrunkenRush::GameControl::SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
      [ "DrunkenRush::GameControl::ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ],
      [ "DrunkenRush::GameControl::ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "DrunkenRush::GameControl::MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
      [ "DrunkenRush::GameControl::MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
      [ "DrunkenRush::GameControl::NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
      [ "DrunkenRush::GameControl::SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
      [ "DrunkenRush::GameControl::ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ]
    ] ]
];