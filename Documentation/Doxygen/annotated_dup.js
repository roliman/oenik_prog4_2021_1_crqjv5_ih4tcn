var annotated_dup =
[
    [ "DrunkenRush", null, [
      [ "GameControl", null, [
        [ "App", "class_drunken_rush_1_1_game_control_1_1_app.html", null ],
        [ "DrunkenRushController", "class_drunken_rush_1_1_game_control_1_1_drunken_rush_controller.html", "class_drunken_rush_1_1_game_control_1_1_drunken_rush_controller" ],
        [ "HighscoreVM", "class_drunken_rush_1_1_game_control_1_1_highscore_v_m.html", "class_drunken_rush_1_1_game_control_1_1_highscore_v_m" ],
        [ "MainWindow", "class_drunken_rush_1_1_game_control_1_1_main_window.html", null ],
        [ "MenuWindow", "class_drunken_rush_1_1_game_control_1_1_menu_window.html", null ],
        [ "MyIoc", "class_drunken_rush_1_1_game_control_1_1_my_ioc.html", "class_drunken_rush_1_1_game_control_1_1_my_ioc" ],
        [ "NameFormWindow", "class_drunken_rush_1_1_game_control_1_1_name_form_window.html", null ],
        [ "SavedGamesWindow", "class_drunken_rush_1_1_game_control_1_1_saved_games_window.html", null ],
        [ "SavedGameVM", "class_drunken_rush_1_1_game_control_1_1_saved_game_v_m.html", "class_drunken_rush_1_1_game_control_1_1_saved_game_v_m" ],
        [ "ScoresVM", "class_drunken_rush_1_1_game_control_1_1_scores_v_m.html", "class_drunken_rush_1_1_game_control_1_1_scores_v_m" ],
        [ "ScoresWindow", "class_drunken_rush_1_1_game_control_1_1_scores_window.html", null ]
      ] ],
      [ "GameLogic", "namespace_drunken_rush_1_1_game_logic.html", "namespace_drunken_rush_1_1_game_logic" ],
      [ "GameLogicTests", "namespace_drunken_rush_1_1_game_logic_tests.html", "namespace_drunken_rush_1_1_game_logic_tests" ],
      [ "GameModel", "namespace_drunken_rush_1_1_game_model.html", "namespace_drunken_rush_1_1_game_model" ],
      [ "GameRenderer", "namespace_drunken_rush_1_1_game_renderer.html", "namespace_drunken_rush_1_1_game_renderer" ],
      [ "Repository", "namespace_drunken_rush_1_1_repository.html", "namespace_drunken_rush_1_1_repository" ]
    ] ],
    [ "XamlGeneratedNamespace", null, [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ]
];