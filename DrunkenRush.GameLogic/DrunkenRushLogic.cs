﻿// <copyright file="DrunkenRushLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameLogic
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Threading;
    using DrunkenRush.GameModel;
    using DrunkenRush.GameModel.DrawableTiles;
    using DrunkenRush.GameModel.Obstacles;
    using DrunkenRush.Repository;

    /// <summary>
    /// The business logic of game DrunkenRush.
    /// </summary>
    public class DrunkenRushLogic : IGameLogic
    {
        private IGameModel model;
        private MapGenerator mapGenerator;
        private IStorageRepository repository;
        private DispatcherTimer playerLogTimer;

        /// <summary>
        /// Initializes a new instance of the <see cref="DrunkenRushLogic"/> class.
        /// </summary>
        /// <param name="newModel">Game model class.</param>
        /// <param name="repository">Game repository for highscore.</param>
        /// <param name="map">the file name of the text file based map.</param>
        public DrunkenRushLogic(IGameModel newModel, IStorageRepository repository, string map)
        {
            this.model = newModel;
            this.repository = repository;
            this.mapGenerator = new MapGenerator(this.model);
            this.InitializeMap(360, 0, map);
            this.playerLogTimer = new DispatcherTimer();
            this.playerLogTimer.Interval += TimeSpan.FromMilliseconds(50);
            this.playerLogTimer.Tick += this.PlayerLogTimer_Tick;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DrunkenRushLogic"/> class.
        /// </summary>
        /// <param name="repository">Repository.</param>
        /// <param name="newModel">Model.</param>
        public DrunkenRushLogic(IGameModel newModel, IStorageRepository repository)
        {
            this.model = newModel;
            this.repository = repository;
        }

        /// <summary>
        /// Handles game over events.
        /// </summary>
        public event EventHandler GameOver;

        /// <summary>
        /// Method that handles basic player movement.
        /// </summary>
        /// <param name="dx">X shift.</param>
        /// <param name="dy">Y shift.</param>
        public void MovePlayer(double dx, double dy)
        {
            double x = (int)(this.model.Player.CX + dx);
            double y = (int)(this.model.Player.CY + dy);

            double newX = this.RoundUpX(x);
            double newY = this.RoundUpY(y);

            if (this.CheckNextStep(newX, newY))
            {
                this.model.Player = new Player(newX, newY);
                if (GetWherePlayerStands(this.model.Player, this.model.Map) == TileType.Water)
                {
                    if (!this.IsLogWherePlayerStands())
                    {
                        this.model.Player.IsAlive = false;
                        this.repository.StoreHighScore(this.model.PlayerName, this.model.Score);

                        this.GameOver?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
        }

        /// <summary>
        /// This method initializes the starting map.
        /// </summary>
        /// <param name="startingX">the starter x coordinate.</param>
        /// <param name="startingY">the starter y coordinate.</param>
        /// <param name="fname">the map in txt file.</param>
        public void InitializeMap(double startingX, double startingY, string fname)
        {
            string filename = @"..\..\..\Map\" + fname;

            StreamReader sr = new StreamReader(filename);

            string[] lines = sr.ReadToEnd().Replace("\r", string.Empty).Split('\n');

            int mapWidth = int.Parse(lines[0]);
            int mapHeight = int.Parse(lines[1]);

            IsometricTile[,] temp = new IsometricTile[mapHeight, mapWidth];

            double tileX = startingX;
            double tileY = startingY;

            for (int i = 0; i < mapHeight; i++)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    int value = int.Parse(lines[i + 2]);

                    temp[i, j] = NumberToTileType(value, tileX, tileY);

                    tileX -= IsometricTile.TileWidth / 2;
                    tileY -= IsometricTile.TileHeight / 2;
                }

                tileX = startingX;
                tileY = startingY + (IsometricTile.TileHeight * (i + 1));
            }

            this.model.Map = temp;
        }

        /// <summary>
        /// Method that calls methods when the map moves.
        /// </summary>
        public void ForwardMove()
        {
            this.MoveObjects(this.model.Objects);
            this.MoveMap();
            this.ClearInvisibleObjects();
        }

        /// <summary>
        /// One tick of the main timer.
        /// </summary>
        public void OneTick()
        {
            foreach (var item in this.model.Objects)
            {
                if (item is Log || item is Car)
                {
                    this.ObjectTick(item);
                }
            }

            if (this.model.Player.CY + Player.PlayerTileHeight < 0 || this.model.Player.CX + Player.PlayerTileWidth < 0)
            {
                this.GameOver?.Invoke(this, EventArgs.Empty);
            }

            if (this.model.KeyDownWatch.ElapsedMilliseconds >= 8000)
            {
                this.model.Player.IsAlive = false;
                this.repository.StoreHighScore(this.model.PlayerName, this.model.Score);

                this.GameOver?.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// This method provide to make the map unlimited.
        /// </summary>
        public void MoveMap()
        {
            IsometricTile[,] temp = this.model.Map;

            int mapWidth = temp.GetLength(1);
            int mapHeight = temp.GetLength(0) - 1;

            for (int i = mapHeight; i > 0; i--)
            {
                for (int j = 0; j < mapWidth; j++)
                {
                    temp[i, j].Brush = temp[i - 1, j].Brush;
                    temp[i, j].Type = temp[i - 1, j].Type;
                }
            }

            TileType tileType = this.mapGenerator.Tiles.ElementAt(this.mapGenerator.TileIndex);
            for (int y = 0; y < mapWidth; y++)
            {
                temp[0, y] = NumberToTileType((int)tileType, temp[0, y].Area.X, temp[0, y].Area.Y);
            }

            this.mapGenerator.PlaceNewObstacles(temp, this.model.Objects);

            this.model.Map = temp;

            this.mapGenerator.TileIndex++;

            if (this.mapGenerator.Tiles.Count == this.mapGenerator.TileIndex)
            {
                this.mapGenerator.ResetTiles();
                this.mapGenerator.GenerateNextTiles();
            }

            this.model.Score++;

            if (GetWherePlayerStands(this.model.Player, this.model.Map) == TileType.Water)
            {
                if (!this.IsLogWherePlayerStands())
                {
                    this.model.Player.IsAlive = false;
                    this.repository.StoreHighScore(this.model.PlayerName, this.model.Score);
                    this.GameOver?.Invoke(this, EventArgs.Empty);

                    this.model.KeyDownWatch.Stop();
                }
            }
            else
            {
                double newX = this.RoundUpX(this.model.Player.CX);
                double newY = this.RoundUpY(this.model.Player.CY);
                this.model.Player = new Player(newX, newY);

                if (this.playerLogTimer.IsEnabled)
                {
                    this.playerLogTimer.Stop();
                }
            }
        }

        /// <summary>
        /// Method that determines whether the car or log should go up or down based on position.
        /// </summary>
        /// <param name="gameItems">item collection.</param>
        public void GameItemDirection(IEnumerable<GameItem> gameItems)
        {
            if (gameItems != null)
            {
                foreach (var item in gameItems)
                {
                    if (item is Log || item is Car)
                    {
                        if (item.CX < (this.model.GameWidth / 2))
                        {
                            item.Direction = Direction.Down;
                        }
                        else
                        {
                            item.Direction = Direction.Up;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Moves objects in one tick.
        /// </summary>
        /// <param name="item">Selected items.</param>
        public void ObjectTick(GameItem item)
        {
            int tileHeight = IsometricTile.TileHeight;
            int tileWidth = IsometricTile.TileWidth;

            if (item is Car)
            {
                if (!item.IsCollision(this.model.Player))
                {
                    if (item.Direction != Direction.Down && item.CX + Car.CarWidth < 0)
                    {
                        int rowCount = this.model.Map.GetLength(0);
                        item.CX += rowCount * tileWidth / 2;
                        item.CY += rowCount * tileHeight / 2;
                    }
                    else if (item.Direction != Direction.Up && item.CX + Car.CarWidth > this.model.GameWidth * 2)
                    {
                        int rowCount = this.model.Map.GetLength(0);
                        item.CX -= rowCount * tileWidth / 2;
                        item.CY -= rowCount * tileHeight / 2;
                    }
                    else
                    {
                        if (item.Direction == Direction.Down)
                        {
                            item.CX += tileWidth / (2 * item.Speed);
                            item.CY += tileHeight / (2 * item.Speed);
                        }
                        else if (item.Direction == Direction.Up)
                        {
                            item.CX -= tileWidth / (2 * item.Speed);
                            item.CY -= tileHeight / (2 * item.Speed);
                        }
                    }
                }
                else
                {
                    this.model.Player.IsAlive = false;
                    this.repository.StoreHighScore(this.model.PlayerName, this.model.Score);
                    this.model.KeyDownWatch.Stop();

                    this.GameOver?.Invoke(this, EventArgs.Empty);
                }
            }
            else if (item is Log)
            {
                if (item.Direction != Direction.Down && item.CX + Log.LogWidth < 0)
                {
                    int rowCount = this.model.Map.GetLength(0);
                    item.CX += rowCount * tileWidth / 2;
                    item.CY += rowCount * tileHeight / 2;
                }
                else if (item.Direction != Direction.Up && item.CX + Log.LogWidth > this.model.GameWidth * 2)
                {
                    int rowCount = this.model.Map.GetLength(0);
                    item.CX -= rowCount * tileWidth / 2;
                    item.CY -= rowCount * tileHeight / 2;
                }
                else
                {
                    if (item.Direction == Direction.Down)
                    {
                        item.CX += tileWidth / (2 * item.Speed);
                        item.CY += tileHeight / (2 * item.Speed);
                    }
                    else if (item.Direction == Direction.Up)
                    {
                        item.CX -= tileWidth / (2 * item.Speed);
                        item.CY -= tileHeight / (2 * item.Speed);
                    }
                }
            }
        }

        /// <summary>
        /// Method that rounds up X after getting off a log.
        /// </summary>
        /// <param name="x">Parameter to be rounded up.</param>
        /// <returns>Rounded up number.</returns>
        public double RoundUpX(double x)
        {
            int w = IsometricTile.TileWidth / 2;

            if (x % w != 0)
            {
                double res = x % w;
                double sub;
                if (Math.Abs(res) > Math.Abs(res - w) || Math.Abs(res) == Math.Abs(res - w))
                {
                    sub = (x % w) - w;
                    return x - sub;
                }
                else
                {
                    sub = x - res;
                    return sub;
                }
            }
            else
            {
                return x;
            }
        }

        /// <summary>
        /// Method that rounds up Y after getting off a log.
        /// </summary>
        /// <param name="y">Parameter to be rounded up.</param>
        /// <returns>Rounded up number.</returns>
        public double RoundUpY(double y)
        {
            int h = IsometricTile.TileHeight / 2;

            if (y % h != 0)
            {
                double res = y % h;
                double sub;
                if (Math.Abs(res) > Math.Abs(res - h) || Math.Abs(res) == Math.Abs(res - h))
                {
                    sub = (y % h) - h;
                    return y - sub;
                }
                else
                {
                    sub = y - res;
                    if (sub == 240)
                    {
                        return 260;
                    }

                    return sub;
                }
            }
            else
            {
                return y;
            }
        }

        /// <summary>
        /// This method aligns objects for their new position in case of forward move.
        /// </summary>
        /// <param name="collection">Collection of moving objects.</param>
        public void MoveObjects(IEnumerable<GameItem> collection)
        {
            int h = IsometricTile.TileHeight;
            int w = IsometricTile.TileWidth;

            foreach (var item in collection)
            {
                if (item.CY + item.GetObjectHeight > (this.model.GameHeight * 2))
                {
                    item.IsVisible = false;
                }
                else
                {
                    item.CX -= w / 2;
                    item.CY += h / 2;
                }
            }
        }

        /// <summary>
        /// Returns whether the player stands on a log or not.
        /// </summary>
        /// <returns>If it is on a log or not.</returns>
        public bool IsLogWherePlayerStands()
        {
            foreach (var item in this.model.Objects)
            {
                if (item is Log)
                {
                    if (item.RealArea.FillContains(this.model.Player.PlayerPoint))
                    {
                        if (!this.playerLogTimer.IsEnabled)
                        {
                            this.playerLogTimer.Start();
                        }

                        return true;
                    }
                    else
                    {
                        if (this.playerLogTimer.IsEnabled)
                        {
                            this.playerLogTimer.Stop();
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks next step.
        /// </summary>
        /// <param name="newX">newX.</param>
        /// <param name="newY">newY.</param>
        /// <returns>Whether the player can step or not.</returns>
        public bool CheckNextStep(double newX, double newY)
        {
            int h = ((Player.PlayerTileHeight / 4) * 3) + 10;
            int w = Player.PlayerTileWidth / 2;

            Point p = new Point(newX + w, newY + h);

            if (newX > this.model.GameWidth || newX < 0 || newY > this.model.GameHeight || newY < 0)
            {
                return false;
            }
            else
            {
                foreach (var obj in this.model.Objects)
                {
                    if (obj is Rock || obj is Tree)
                    {
                        if (obj.RealArea.FillContains(p))
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Clears the invisible objects from the specified list.
        /// </summary>
        public void ClearInvisibleObjects()
        {
            foreach (var item in this.model.Objects.ToList())
            {
                if (!item.IsVisible)
                {
                    this.model.Objects.Remove(item);
                }
            }
        }

        /// <summary>
        /// This method tells what type of tile have to draw to the screen, from a number that gets from a txt-file called "map".
        /// </summary>
        /// <param name="value">the number of type.</param>
        /// <param name="x">the tile x coordinate.</param>
        /// <param name="y">the tile y coordinate.</param>
        /// <returns>Isometric tile.</returns>
        private static IsometricTile NumberToTileType(int value, double x, double y)
        {
            switch (value)
            {
                case 0: return new IsometricTile(x, y, GrassTile.Type, GrassTile.Brush);
                case 1: return new IsometricTile(x, y, AsphaltTile.Type, AsphaltTile.Brush);
                case 2: return new IsometricTile(x, y, WaterTile.Type, WaterTile.Brush);
                case 3: return new IsometricTile(x, y, DirtTile.Type, DirtTile.Brush);

                default: throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Gets the type of the tile where the player stands.
        /// </summary>
        /// <param name="player">the player.</param>
        /// <param name="tiles">the map.</param>
        /// <returns>TileType.</returns>
        private static TileType GetWherePlayerStands(Player player, IsometricTile[,] tiles)
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    if (tiles[i, j].Area.Contains(player.PlayerPoint))
                    {
                        return tiles[i, j].Type;
                    }
                }
            }

            throw new KeyNotFoundException();
        }

        private void PlayerLogTimer_Tick(object sender, EventArgs e)
        {
            this.MovePlayerWithLog();
        }

        /// <summary>
        /// Move player with log.
        /// </summary>
        private void MovePlayerWithLog()
        {
            int h = IsometricTile.TileHeight;
            int w = IsometricTile.TileWidth;
            int ls = Log.LogSpeed;

            this.model.Player = new Player(this.model.Player.CX - (w / (2 * ls)), this.model.Player.CY - (h / (2 * ls)));
        }
    }
}