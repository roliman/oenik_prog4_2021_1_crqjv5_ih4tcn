﻿// <copyright file="IGameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrunkenRush.GameModel;

    /// <summary>
    /// Interface for business logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Handles game over events.
        /// </summary>
        event EventHandler GameOver;

        /// <summary>
        /// Method that handles basic player movement.
        /// </summary>
        /// <param name="dx">X shift.</param>
        /// <param name="dy">Y shift.</param>
        void MovePlayer(double dx, double dy);

        /// <summary>
        /// This method provide to make the map unlimited.
        /// </summary>
        void MoveMap();

        /// <summary>
        /// Method that determines wether the car or log should go up or down based on position.
        /// </summary>
        /// <param name="gameItems">game items.</param>
        void GameItemDirection(IEnumerable<GameItem> gameItems);

        /// <summary>
        /// One tick of the main timer.
        /// </summary>
        void OneTick();

        /// <summary>
        /// Moves objects in one tick.
        /// </summary>
        /// <param name="item">Selected items.</param>
        void ObjectTick(GameItem item);

        /// <summary>
        /// This method initializes the starting map.
        /// </summary>
        /// <param name="startingX">the starter x coordinate.</param>
        /// <param name="startingY">the starter y coordinate.</param>
        /// <param name="fname">the map in txt file.</param>
        void InitializeMap(double startingX, double startingY, string fname);

        /// <summary>
        /// Method that calls methods when the map moves.
        /// </summary>
        void ForwardMove();

        /// <summary>
        /// Checks next step.
        /// </summary>
        /// <param name="newX">newX.</param>
        /// <param name="newY">newY.</param>
        /// <returns>Whether the player can step or not.</returns>
        bool CheckNextStep(double newX, double newY);

        /// <summary>
        /// Returns whether the player stands on a log or not.
        /// </summary>
        /// <returns>If it is on a log or not.</returns>
        bool IsLogWherePlayerStands();
    }
}