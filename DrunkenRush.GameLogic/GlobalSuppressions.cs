﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameLogic.MapGenerator.GenerateNextTiles")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameLogic.MapGenerator.GenerateGrassArea")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameLogic.MapGenerator.PlaceNewObstacles(DrunkenRush.GameModel.IsometricTile[,],System.Collections.Generic.List{DrunkenRush.GameModel.GameItem})")]
[assembly: SuppressMessage("Security", "CA5394:Do not use insecure randomness", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameLogic.MapGenerator.GenerateDirtArea")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "this", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.StoreNewHighscore~System.Boolean")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.InitializeMap(System.Double,System.Double,System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1404:Code analysis suppression should have justification", Justification = "<Pending>")]
[assembly: SuppressMessage("Globalization", "CA1307:Specify StringComparison for clarity", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.InitializeMap(System.Double,System.Double,System.String)")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.InitializeMap(System.Double,System.Double,System.String)")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.InitializeMap(System.Double,System.Double,System.String)")]
[assembly: SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1202:Elements should be ordered by access", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.MovePlayer(System.Double,System.Double)")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.GetWherePlayerStands(DrunkenRush.GameModel.Player,DrunkenRush.GameModel.IsometricTile[,])~DrunkenRush.GameModel.TileType")]
[assembly: SuppressMessage("Performance", "CA1814:Prefer jagged arrays over multidimensional", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.MapGenerator.PlaceNewObstacles(DrunkenRush.GameModel.IsometricTile[,],System.Collections.Generic.List{DrunkenRush.GameModel.GameItem})")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant", Justification = "There are places where unsigned values are used, which is considered not Cls compliant.")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.RoundUpX(System.Double)~System.Double")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.RoundUpY(System.Double)~System.Double")]
[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "<Pending>", Scope = "member", Target = "~M:DrunkenRush.GameLogic.DrunkenRushLogic.MoveObjects(System.Collections.Generic.IEnumerable{DrunkenRush.GameModel.GameItem})")]
