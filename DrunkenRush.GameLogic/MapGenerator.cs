﻿// <copyright file="MapGenerator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DrunkenRush.GameLogic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DrunkenRush.GameModel;
    using DrunkenRush.GameModel.Obstacles;

    /// <summary>
    /// Helper class for map generating.
    /// </summary>
    internal class MapGenerator
    {
        private static Random rand = new Random();

        private IGameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="MapGenerator"/> class.
        /// </summary>
        /// <param name="model">model.</param>
        public MapGenerator(IGameModel model)
        {
            this.model = model;
            this.Tiles = new List<TileType>();
            this.GenerateNextTiles();
        }

        /// <summary>
        /// Gets next map tiles.
        /// </summary>
        public List<TileType> Tiles { get; private set; }

        /// <summary>
        /// Gets or Sets the last index of tiles.
        /// </summary>
        public int TileIndex { get; set; }

        /// <summary>
        /// Generates the next map.
        /// </summary>
        public void GenerateNextTiles()
        {
            this.TileIndex = 0;

            bool asphalt = false;
            bool grass = false;
            bool dirt = false;
            bool water = false;

            // while not all type used at least once
            while (!asphalt || !grass || !dirt || !water)
            {
                int number = rand.Next(0, 4);

                if (!asphalt && TileSwitch(number) == TileType.Asphalt)
                {
                    this.GenerateRoad();
                    asphalt = true;
                }
                else if (!water && TileSwitch(number) == TileType.Water)
                {
                    this.GenerateRiver();
                    water = true;
                }
                else if (!grass && TileSwitch(number) == TileType.Grass)
                {
                    this.GenerateGrassArea();
                    grass = true;
                }
                else if (!dirt && TileSwitch(number) == TileType.Dirt)
                {
                    this.GenerateDirtArea();
                    dirt = true;
                }
            }
        }

        /// <summary>
        /// This method places obstacles on the newly generated row.
        /// </summary>
        /// <param name="map">game map.</param>
        /// <param name="list">obstacles.</param>
        public void PlaceNewObstacles(IsometricTile[,] map, List<GameItem> list)
        {
            int columnCount = map.GetLength(1);

            TileType type = this.Tiles[this.TileIndex];

            if (type == TileType.Grass || type == TileType.Dirt)
            {
                int randomIndex = rand.Next(0, columnCount - 1);

                switch (type)
                {
                    case TileType.Grass: list.Add(new Rock(map[0, randomIndex].Area.X, map[0, randomIndex].Area.Y));
                        break;
                    case TileType.Dirt: list.Add(new Tree(map[0, randomIndex].Area.X, map[0, randomIndex].Area.Y));
                        break;
                    default:
                        break;
                }
            }
            else
            {
                if (type == TileType.Water)
                {
                    list.Add(new Log(map[0, 0].Area.X, map[0, 0].Area.Y));

                    list.Add(new Log(map[0, columnCount - 1].Area.X, map[0, columnCount - 1].Area.Y));
                }
                else
                {
                    if (this.TileIndex % 2 == 0)
                    {
                        if (rand.Next(0, 2) % 2 == 0)
                        {
                            list.Add(new Car(map[0, 0].Area.X, map[0, 0].Area.Y - (IsometricTile.TileHeight / 2), Direction.Up));
                        }
                        else
                        {
                            list.Add(new Car(map[0, columnCount - 1].Area.X, map[0, columnCount - 1].Area.Y - (IsometricTile.TileHeight / 2), Direction.Down));
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Resets the next map.
        /// </summary>
        public void ResetTiles()
        {
            this.Tiles.Clear();
        }

        private static TileType TileSwitch(int value)
        {
            switch (value)
            {
                case 0: return TileType.Grass;
                case 1: return TileType.Asphalt;
                case 2: return TileType.Water;
                case 3: return TileType.Dirt;

                default: throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Generates asphalt type rows.
        /// </summary>
        private void GenerateRoad()
        {
            // roads always 4 tile wide
            for (int i = 0; i < 4; i++)
            {
                this.Tiles.Add(TileType.Asphalt);
            }
        }

        /// <summary>
        /// Generates grass type roads.
        /// </summary>
        private void GenerateGrassArea()
        {
            int number = rand.Next(1, 6);

            for (int i = 0; i < number; i++)
            {
                this.Tiles.Add(TileType.Grass);
            }
        }

        /// <summary>
        /// Generates river type rows.
        /// </summary>
        private void GenerateRiver()
        {
            this.Tiles.Add(TileType.Grass);

            // river always 2 tile wide
            for (int i = 0; i < 2; i++)
            {
                this.Tiles.Add(TileType.Water);
            }

            this.Tiles.Add(TileType.Dirt);
        }

        /// <summary>
        /// Generates dirt type rows.
        /// </summary>
        private void GenerateDirtArea()
        {
            int number = rand.Next(0, 4);

            for (int i = 0; i < number; i++)
            {
                this.Tiles.Add(TileType.Dirt);
            }
        }
    }
}
